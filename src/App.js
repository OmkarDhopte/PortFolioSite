import { Box, Flex, Grid, Heading, Link,List, ListItem,UnorderedList,Image, Text,Icon, Button,chakra, shouldForwardProp, ListIcon } from "@chakra-ui/react";
import { motion, isValidMotionProp,useScroll } from 'framer-motion';
import { FaGitlab,FaLinkedin } from "react-icons/fa6";
import { BiLogoGmail } from "react-icons/bi";
import BGPage from './Images/BGPage.jpg'
import { IoReorderFourOutline } from "react-icons/io5";
import RSTStore1 from './Images/RST_Store.jpg'
import RSTOrderPage from './Images/OrderPage.jpg'
import RSTProductScreen from './Images/ProductScreen.jpg'
import Omkar from './Images/Omkar.jpg'
import Resume from './Images/updatedResumeFinal.pdf'
import {useState} from 'react'
import Survellence1 from './Images/Survellence.jpg'
import TESSAboutUs from './Images/TESSAboutus.jpg'
import TESSAdminPanel from './Images/TESSAdminPannel.jpg'


function App() {
  const [show, setShow] = useState(false);
  const [RSTCert, setRSTCert] = useState(1)
  const [DegreeCert, setDegreeCert] = useState(1)
  const Color1 = 'rgba(66, 37, 159, 0.5)'
  const { scrollYProgress } = useScroll()
  const ChakraBox = chakra(motion.div, {
    /**
     * Allow motion props and non-Chakra props to be forwarded.
     */
    shouldForwardProp: (prop) => isValidMotionProp(prop) || shouldForwardProp(prop),
  });

  return (
    <>
      <ChakraBox 
      backgroundImage={BGPage} 
      backgroundSize='cover'
      >
        <ChakraBox position='fixed' height='100%' width='5px' style={{ scaleY: scrollYProgress }} backgroundColor='red' />
        <Box>
          <Grid gridTemplateColumns='7fr 6fr' backgroundRepeat='no-repeat' backgroundSize='cover' textColor='rgba(60, 65, 66, 1)' textShadow='3px 3px rgba(214, 188, 250, 0.3)' padding='50px'>
            <Heading as='h1' textColor='rgba(60, 65, 66, 1)' fontFamily='Raleway' paddingLeft='25px'>
              OMKAR DHOPTE
            </Heading>
            <Box
              paddingLeft={{base:'50%',md:'80px'}}
				      display={{ base: 'block', md: 'none' }}
				      onClick={() => setShow(!show)}>
				      <Icon as={IoReorderFourOutline} color='white' w='6' h='6' />
			      </Box>
            <Flex justifyContent='space-between' display={{ base: show ? 'block' : 'none', md: 'flex' }}
				      width={{ base: 'full', md: 'auto' }}
				      mt={{ base: '5', md: '0' }} fontFamily='Raleway'>
              <Box _hover={{color:'#E53E3E', textDecoration:'none'}}>
                <Link href="#About">About</Link>
              </Box>
              <Box _hover={{color:'#E53E3E', textDecoration:'none'}}>
                <Link href="#Skills">Skills</Link>
              </Box>
              <Box _hover={{color:'#E53E3E', textDecoration:'none'}}>
                <Link href="#Projects">Projects</Link>
              </Box>
              <Box _hover={{color:'#E53E3E', textDecoration:'none'}}>
                <Link href="#Education">Education & Certification</Link>
              </Box>
              <Box _hover={{color:'#E53E3E', textDecoration:'none'}}>
                <Text as='b'>
                  <Link href={Resume} download={Resume}>Download Resume</Link>
                </Text>                
              </Box>
            </Flex>
          </Grid>
        </Box>
        <Flex flexDirection='column' >
          <Box padding='50px' margin='60px 0px 60px 60px' fontSize='18px' background='radial-gradient(circle at center, #303030 , #000000)'>
            <Box>
              <Flex flexDirection={{ base:  'column', md: 'row'}} padding={{base:'none',md:'50px'}} fontFamily='Raleway'id="About" >
                <Image src={Omkar} height={{ base: '100%', md: '20%'}} width={{ base: '100%', md: '40%'}} />
                <ChakraBox 
                position='relative' 
                textAlign='left' 
                right={{base:'none',md:'50px'}}
                animate={{scale:[2,1]}}
                transition={{duration:1.5}}
                >
                  <Text fontSize={{ base: '140%', md: '90px'}} textColor='rgba(255, 255, 0, 1)' as='b'>I'M OMKAR</Text><br/>
                  <Text fontSize={{ base: '100%', md: '60px'}} textColor='rgba(255, 255, 0, 0.9)'>A DEDICATED FULL STACK DEVELOPER.</Text>
                  <Text fontSize={{ base: '80%', md: '40px'}} textColor='rgba(255, 255, 0, 0.7)'>Based in India.</Text>
                  <Text fontSize={{ base: '80%', md: '25px'}} textColor='rgba(255, 255, 0, 0.7)'>I have a passion for building robust and scalable applications using React and Node.js. If you have a great project that needs some amazing skills, I’m your guy.</Text>
                </ChakraBox>
              </Flex>
              <br/>
              <br/>
              <Box>
                <Heading marginTop={{base:'10%',md:'100px'}} as='h2' id="Skills" marginBottom='60px'textColor='rgba(255, 255, 0, 1)' fontSize={{base:'100%', md:'60px'}} fontFamily='raleway'>
                  TECHNICAL SKILLS 
                </Heading>
                  <Flex flexDir={{base:'column', md:'row'}} columnGap={{base:'none', md:'30px'}} rowGap={{base:'30px', md:'none'}} marginLeft={{base:'0px', md:'10px'}} justifyContent={{base:'left', md:'center'}}  >


                    <ChakraBox backgroundImage={BGPage} height={{base:'150%', md:'300px'}} width={{base:'100%', md:'33%'}} borderRadius='20px' backgroundSize='cover' backgroundRepeat='no-repeat' textAlign='center' whileInView={{scale:[0.5,1]}}
                      transition={{type:"spring", stiffness: '80',duration: 1, ease:'linear',delay: 0.1}}>
                      <Box as='p' padding='30px' fontSize={{base:'80%', md:'25px'}}>
                        FRONT END:
                      </Box>
                      <List textAlign='left' paddingLeft={{base:'10%', md:'120px'}} fontSize={{base:'small'}} textColor={{base:'white'}} paddingBottom={{base:'20%', md:'none'}}>
                        <UnorderedList spacing='10px'>
                          <ListItem>React.js</ListItem>
                          <ListItem>HTML5</ListItem>
                          <ListItem>CSS3</ListItem>
                          <ListItem>Responsive Design</ListItem>
                        </UnorderedList>
                      </List>
                    </ChakraBox>
                    <ChakraBox backgroundImage={BGPage} height={{base:'150%', md:'300px'}} width={{base:'100%', md:'33%'}} borderRadius='20px' backgroundSize='cover' backgroundRepeat='no-repeat' textAlign='center' whileInView={{scale:[0.5,1]}}
                      transition={{type:"spring", stiffness: '80',duration: 1, ease:'linear',delay: 0.1}}>
                      <Box as='p' padding='30px' fontSize={{base:'80%', md:'25px'}}>
                        BACK END:
                      </Box>
                      <List textAlign='left' paddingLeft={{base:'10%', md:'120px'}} fontSize={{base:'small'}} textColor={{base:'white'}} paddingBottom={{base:'20%', md:'none'}}>
                        <UnorderedList spacing='10px'>
                          <ListItem>Node.js</ListItem>
                          <ListItem>Express.js</ListItem>
                          <ListItem>Database Management <br/> (MongoDB)</ListItem>
                        </UnorderedList>
                      </List>
                    </ChakraBox>
                    <ChakraBox backgroundImage={BGPage} height={{base:'110%', md:'300px'}} width={{base:'100%', md:'33%'}} borderRadius='20px' backgroundSize='cover' backgroundRepeat='no-repeat' textAlign='center' whileInView={{scale:[0.5,1]}}
                      transition={{type:"spring", stiffness: '80',duration: 1, ease:'linear',delay: 0.1}}>
                      <Box as='p' padding='30px' fontSize={{base:'80%', md:'25px'}}>
                        GENERAL
                      </Box>
                      <List textAlign='left' paddingLeft={{base:'10%', md:'120px'}} fontSize={{base:'small'}} textColor={{base:'white'}} paddingBottom={{base:'20%', md:'none'}}>
                        <UnorderedList spacing='10px'>
                          <ListItem>JavaScript (ES6+)</ListItem>
                          <ListItem>Git Version Control</ListItem>
                          <ListItem>Agile/Scrum Methodology</ListItem>
                          <ListItem>Problem-Solving</ListItem>
                        </UnorderedList>
                      </List>
                    </ChakraBox>
                  </Flex>
              </Box>
            </Box>
            <br/>
            <br/>
            <hr/>
            <Heading marginTop='100px' as='h2' id="Projects" marginBottom={{base:'60px', md:'10px'}} textColor='rgba(255, 255, 0, 1)' fontSize={{base: '140%',md:'60px'}} fontFamily='raleway'>
                PROJECTS
            </Heading>
            <Box marginTop='50px'>
              <Flex flexDir='column'>
                <ChakraBox textColor='rgba(255, 255, 0, 1)' textAlign={{base:'none',md:'right'}} fontSize={{base:'larger',md:'40px'}} paddingBottom={{base:'20%', md:'5%'}}>
                  RST STORE
                </ChakraBox>
                <Grid gridTemplateColumns={{base:'1fr',md:'2fr 2fr'}} columnGap='50px'>
                  <Grid gridTemplateRows='2fr 3fr' rowGap='50px' >
                    <Image src={RSTProductScreen} borderRadius='5%'/>
                    <Image src={RSTOrderPage} borderRadius='5%'/>
                  </Grid>
                  <Grid gridTemplateRows={{base:'3fr',md:'0.3fr 3fr'}} rowGap='50px'>
                    <ChakraBox textAlign='justify' textColor='#fff41f' whileInView={{scale:[0.5,1]}}>
                      RST Store is an imaginary online clothing store designed to provide users with a seamless and enjoyable shopping experience.  This project was conceived to demonstrate my proficiency in creating visually appealing and functional e-commerce websites.
                      <br/><br/>
                      <Link href="http://64.227.141.130/" color='#fff41f' target='_blank'>
                        <Button bg='#45289f' textColor='white ' _hover={{bg:'rgba(69, 40, 159, 0.5)'}}>To Visit Site</Button>
                      </Link>
                    </ChakraBox>
                    <Image src={RSTStore1} borderRadius='8%'/>
                  </Grid>
                </Grid>
              </Flex>
            </Box>
            <Box marginTop={{base:'100px',md:'none'}}>
              <Flex flexDir='column'>
                <ChakraBox textColor='rgba(255, 255, 0, 1)' textAlign={{base:'none',md:'left'}} fontSize={{base:'larger',md:'40px'}} paddingBottom={{base:'20%', md:'5%'}}>
                  T.E.S.S.
                </ChakraBox>
                <Grid gridTemplateColumns={{base:'2fr',md:'0.5fr 0.5fr'}} columnGap='50px'>
                  <Grid gridTemplateRows={{base:'3fr',md:'0.6fr 4fr'}} rowGap='50px'>
                    <ChakraBox textAlign='justify' textColor='#fff41f' whileInView={{scale:[0.5,1]}}>
                        T.E.S.S. offer comprehensive solutions for real-time monitoring, video recording, and data analysis to enhance security and safety across various environments. Using advanced technologies such as cameras, sensors, and analytics, these services provide organizations with effective tools for threat detection, incident response, and the safeguarding of people and assets.
                      <br/><br/>
                      <Link href="https://tessallcapitalcity.com/" color='#fff41f' target='_blank'>
                        <Button bg='#45289f' textColor='white ' _hover={{bg:'rgba(69, 40, 159, 0.5)'}}>To Visit Site</Button>
                      </Link>
                    </ChakraBox>
                    <Image src={Survellence1} borderRadius='8%' marginBottom={{base:'50px',md:'none'}}/>
                  </Grid>
                  <Grid gridTemplateRows='0.3fr 1fr' rowGap='30px'>
                    <Image src={TESSAdminPanel} borderRadius='5%'/>
                    <Image src={TESSAboutUs} borderRadius='5%'/>
                  </Grid>
                </Grid>
              </Flex>
            </Box>
            <Box>
              <Heading as='h2' id="Education" fontSize={{base:'100%', md: '60px'}} textColor='#fff41f' marginBottom={{base:'10%',md:'none'}} fontFamily='raleway'>
                EDUCATION AND CERTIFICATION
              </Heading>
              <Box as='section' className='first'>
              <Grid 
              gridTemplateColumns={{base:'2fr', md: '3fr 2fr'}} 
              gridColumnGap={{base:'none', md: '100px'}}
              gridRowGap={{base:'none', md: '100px'}}
              >
                  <Flex flexDirection='column' textColor='#fff41f' onMouseEnter={()=>setTimeout(() => {
                    setRSTCert(0) }, 200)} onMouseLeave={()=>setTimeout(() => {
                      setRSTCert(1) }, 200)}>
                    <Text fontSize={{base:'90%', md: '40px'}}>FULL STACK CERTIFICATION</Text>
                    <List fontSize={{base:'70%', md: '18px'}} marginBottom='20%'>
                      <ListItem><Text as='b'>Completed At : </Text> 2023</ListItem>
                      <ListItem><Text as='b'>University : </Text>RST Forum</ListItem>
                    </List>
                  </Flex>
                  <Box backgroundImage={RSTCert === 0 ? BGPage : Color1 } backgroundColor={RSTCert === 1 ? Color1 : BGPage} height={{base:'100%', md:'250px'}} width={{base:'100%', md:'350px'}} borderRadius='20px' backgroundSize='cover' backgroundRepeat='no-repeat' textAlign={{base:'left', md: 'center'}} textColor={RSTCert === 0 ? 'none': 'rgba(255, 244, 31, 0.7)'} fontSize={{base:'60%', md: '18px'}}>
                    <List paddingTop='50px' textAlign={{base:'none', md: 'left'}} paddingLeft={{base:'10%', md:'50px'}} paddingBottom={{base:'25%', md:'none'}}>
                      <UnorderedList>
                        <ListItem>Front-End Development</ListItem>
                        <ListItem>Back-End Development</ListItem>
                        <ListItem>Version Contro (Git)</ListItem>
                        <ListItem>Project Management</ListItem>
                        <ListItem>Soft Skills</ListItem>
                      </UnorderedList>
                    </List>
                  </Box>
                </Grid>
              </Box>
              <Box as='section' className='second' marginTop='50px'>
              <Grid 
              gridTemplateColumns={{base:'2fr', md: '3fr 2fr'}} 
              gridColumnGap={{base:'none', md: '100px'}}
              gridRowGap={{base:'none', md: '100px'}}
              >
                  <Flex flexDirection='column' textColor='#fff41f' onMouseEnter={()=>setTimeout(() => {
                    setDegreeCert(0) }, 200)} onMouseLeave={()=>setTimeout(() => {
                      setDegreeCert(1) }, 200)}>
                    <Text fontSize={{base:'100%', md: '40px'}}>B.E IN EXTC</Text>
                    <List fontSize={{base:'80%', md: '18px'}} marginBottom='20%'>
                      <ListItem><Text as='b'>Completed At : </Text> 2022</ListItem>
                      <ListItem><Text as='b'>University : </Text>Mumbai University</ListItem>
                    </List>
                  </Flex>
                  <Box backgroundImage={DegreeCert === 0 ? BGPage : Color1 } backgroundColor={DegreeCert === 1 ? Color1 : BGPage} height={{base:'100%', md:'250px'}} width={{base:'100%', md:'350px'}} borderRadius='20px' backgroundSize='cover' backgroundRepeat='no-repeat' textAlign={{base:'left', md: 'center'}} textColor={DegreeCert === 0 ? 'none': 'rgba(255, 244, 31, 0.7)'} fontSize={{base:'60%', md: '18px'}}>
                    <List paddingTop='40px' textAlign={{base:'none', md: 'left'}} paddingLeft={{base:'10%', md:'50px'}} paddingBottom={{base:'25%', md:'none'}}>
                      <UnorderedList fontSize={{base:'100%', md: '18px'}}>
                        <ListItem>Circuit Analysis and Design</ListItem>
                        <ListItem>Digital Electronics</ListItem>
                        <ListItem>Signal Processing</ListItem>
                        <ListItem>Project Management</ListItem>
                        <ListItem>Communication Systems</ListItem>
                        <ListItem>Networks</ListItem>
                      </UnorderedList>
                    </List>
                  </Box>
                </Grid>
              </Box>
            </Box>
          </Box>
        </Flex>
        <Flex padding='5%' backgroundColor='#805AD5' borderRadius='60% 22% 60%' textColor='rgba(214, 188, 250, 1)' justifyContent='space-between'>
          <List listStyleType='none' overflow='hidden' display='block'>
            <ListItem float='left' padding='20px' _hover={{color:'#fff41f'}}>
              <Link href='https://gitlab.com/OmkarDhopte' target='_blank'>
                <ListIcon as={FaGitlab}/>
                GitLab
              </Link>
            </ListItem>
            <ListItem float='left' padding='20px' _hover={{color:'#fff41f'}}>
              <Link href='https://www.linkedin.com/in/omkar-dhopte-256055233/' target='_blank'>
                <ListIcon as={FaLinkedin}/>
                linkedin
              </Link>
            </ListItem>
            <ListItem float='left' padding='20px' _hover={{color:'#fff41f'}}>
              <Link href='mailto:omkardhopte951@gmail.com' target='_blank'>
                <ListIcon as={BiLogoGmail}/>
                Gmail
              </Link>
            </ListItem>
          </List>
          <Link href='#'>
            <Button bg='#805AD5' textColor='#fff41f' _hover={{bg:'#fff41f',textColor:'#805AD5'}}>
                To Top
            </Button>
          </Link>
        </Flex>
      </ChakraBox>
    </>
  );
}

export default App;
